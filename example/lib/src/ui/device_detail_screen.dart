import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:flutter_reactive_ble_example/src/ble/ble_device_connector.dart';
import 'package:flutter_reactive_ble_example/src/ui/device_services_screen.dart';
import 'package:provider/provider.dart';
import 'package:flutter_reactive_ble_example/src/ui/common_gatt.dart';

class DeviceDetailScreen extends StatelessWidget {
  final DiscoveredDevice device;

  const DeviceDetailScreen({@required this.device}) : assert(device != null);

  @override
  Widget build(BuildContext context) =>
      Consumer2<BleDeviceConnector, ConnectionStateUpdate>(
        builder: (_, deviceConnector, connectionStateUpdate, __) =>
            _DeviceDetail(
          device: device,
          connectionUpdate: connectionStateUpdate != null &&
                  connectionStateUpdate.deviceId == device.id
              ? connectionStateUpdate
              : ConnectionStateUpdate(
                  deviceId: device.id,
                  connectionState: DeviceConnectionState.disconnected,
                  failure: null,
                ),
          connect: deviceConnector.connect,
          disconnect: deviceConnector.disconnect,
          discoverServices: deviceConnector.discoverServices,
        ),
      );
}

class _DeviceDetail extends StatelessWidget {
  const _DeviceDetail({
    @required this.device,
    @required this.connectionUpdate,
    @required this.connect,
    @required this.disconnect,
    @required this.discoverServices,
    Key key,
  })  : assert(device != null),
        assert(connectionUpdate != null),
        assert(connect != null),
        assert(disconnect != null),
        assert(discoverServices != null),
        super(key: key);

  final DiscoveredDevice device;
  final ConnectionStateUpdate connectionUpdate;
  final void Function(String deviceId) connect;
  final void Function(String deviceId) disconnect;
  // final void Function(String deviceId) discoverServices;     // todo - old prior to making variable
  final Future<List<DiscoveredService>> Function(String deviceId)
      discoverServices;

  bool _deviceConnected() =>
      connectionUpdate.connectionState == DeviceConnectionState.connected;

  String _filterGattUuids(String uuid) => uuid.substring(4, 8);

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () async {
          disconnect(device.id);
          return true;
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text(device.name ?? "unknown"),
          ),
          body: Center(
            child: SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "ID: ${connectionUpdate.deviceId}",
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Status: ${connectionUpdate.connectionState}",
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                          onPressed: !_deviceConnected()
                              ? () => connect(device.id)
                              : null,
                          child: const Text("Connect"),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                          onPressed: _deviceConnected()
                              ? () => disconnect(device.id)
                              : null,
                          child: const Text("Disconnect"),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                          // onPressed: _deviceConnected()                    // *** original code from example
                          //     ? () => discoverServices(device.id)          // *** original code from example
                          //     : null,                                      // *** original code from example
                          onPressed: () async {
                            if (_deviceConnected()) {
                              // *** My attempt at storing as a variable (this entire anonymous function)
                              // var services = await discoverServices(device.id);
                              var services = await discoverServices(device.id);
                              print(
                                  "services: $services"); // *** services is underlined with an error: This expression has a type of 'void' so its value can't be used.
                              print("PRINT BY ELEMENT:\n");
                              for (var i = 0; i < services.length; i++) {
                                // SERVICES
                                DiscoveredService service = services[i];
                                var uuidString = service.serviceId.toString();
                                var filteredUUID =
                                    _filterGattUuids(uuidString).toUpperCase();

                                print(">>>> Service object: $service");
                                // print("Filtered uuid: $filteredUUID");
                                // print(
                                //     "Filtered uuid to UPPERCASE: ${filteredUUID.toUpperCase()}");
                                if (gattMap.containsKey(filteredUUID)) {
                                  print(
                                      "SERVICE: --------- ${gattMap[filteredUUID]} --------------");
                                }
                                print(
                                    "Service ID #${i + 1}: ${service.serviceId}");
                                // CHARACTERISTICS
                                for (var y = 0;
                                    y < service.characteristicIds.length;
                                    y++) {
                                  var characteristicUuid =
                                      service.characteristicIds[y];
                                  var characteristicUuidString =
                                      characteristicUuid.toString();
                                  var filteredCharacteristicUUID =
                                      _filterGattUuids(characteristicUuidString)
                                          .toUpperCase();
                                  if (gattCharacteristics.containsKey(
                                      filteredCharacteristicUUID)) {
                                    print(
                                        "CHARACTERISTIC #${y + 1}: ---------------- ${gattCharacteristics[filteredCharacteristicUUID]} ------------------");
                                  }
                                  print(
                                      "Characteristic ID #${y + 1}: ${service.characteristicIds[y]}");
                                }
                                // INCLUDED SERVICES
                                for (var z = 0;
                                    z < service.includedServices.length;
                                    z++) {
                                  print(
                                      "Included Services: ${service.includedServices}");
                                }
                              }
                              await Navigator.push<void>(
                                context,
                                MaterialPageRoute(
                                  // Below 2 lines work, just trying to iterate through all
                                  builder: (context) =>
                                      DeviceServicesScreen(services),
                                ),
                              );
                            }
                          },
                          child: const Text("Discover Services"),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ), // *** Where I'd like to populate the results of the discoverServices(device.id)
                  Column(
                    children: [
                      // Text("Write to bluetooth device"),
                      TextField(
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "FINDME (0x00, 0x01, 0x02)",
                        ),
                        onSubmitted: (userInput) {
                          var alertLevel = {
                            "0": 0x00,
                            "1": 0x01,
                            "2": 0x02,
                          };
                          final characteristic = QualifiedCharacteristic(
                              serviceId: Uuid([0x1802]), // Immediate Alert
                              characteristicId: Uuid([0x2a06]), // Alert Level
                              deviceId: device.id);
                          print(
                              "characteristic used to write: $characteristic");
                          print("userInput: $userInput");
                          // flutterReactiveBle.writeCharacteristicWithoutResponse(
                          //     characteristic,
                          //     value: [0x00]
                          _deviceConnected();
                          if (alertLevel.containsKey(userInput)) {
                            FlutterReactiveBle()
                                .writeCharacteristicWithoutResponse(
                                    characteristic,
                                    value: [alertLevel[userInput]]);
                          } else
                            print(
                                "Not a valid option. Please enter 0, 1 or 2.");
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      );
}
