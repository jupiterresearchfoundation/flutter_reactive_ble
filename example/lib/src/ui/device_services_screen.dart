import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';

class DeviceServicesScreen extends StatelessWidget {
  final List<DiscoveredService> deviceServiceList;

  DeviceServicesScreen(this.deviceServiceList);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          child: Text(deviceServiceList.toString()),
        ),
      );
}

// class ShowServices extends StatefulWidget {
//   @override
//   _ShowServicesState createState() => _ShowServicesState();
// }
//
// class _ShowServicesState extends State<ShowServices> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: ListTile(),
//     );
//   }
// }
